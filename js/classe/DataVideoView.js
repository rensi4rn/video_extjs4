Ext.ns('Django');
Ext.define('Django.classe.DataVideoView',{
  uses:['Ext.ux.form.SearchField'],
  constructor:function(config){
  	
  	Ext.apply(this,config||{});
  	
  	    Ext.define('Mobile', {
	        extend: 'Ext.data.Model',
	        fields: ['src','srcposter','videotype',
	            {name: 'hasEmail', type: 'bool'},
	            {name: 'hasCamera', type: 'bool'},
	            {name: 'id', type: 'int'},
	            'name',
	            {name: 'price', type: 'int'},
	            'screen',
	            'camera',
	            'color',
	            'type',
	            {name: 'reviews', type: 'int'},
	            {name: 'screen-size', type: 'int'}
	        ]
	    });
	    
	    
	    // create the Data Store
       this.store = Ext.create('Ext.data.Store', {
           model: 'Mobile',
	       remoteGroup: true,
           pageSize: 10,
           proxy: {
	            // load using script tags for cross domain, if the data in on the same domain as
	            // this page, an Ajax proxy would be better
	            type:'ajax',
	            url: 'video.json',
	            filterParam: 'query',
	            encodeFilters: function(filters) {
	                return filters[0].value;
	            },
	            
	            reader: {
	            	type:'json',
	                root: 'topics',
	                totalProperty: 'totalCount'
	            },
	            // sends single sort as multi parameter
	            simpleSortMode: true,
	            // sends single group as multi parameter
	            simpleGroupMode: true,
	
	            // This particular service cannot sort on more than one field, so grouping === sorting.
	            groupParam: 'sort',
	            groupDirectionParam: 'dir'
	        },
        	sorters: [{
	            property: 'name',
	            direction: 'ASC'
	       	 }],
       	    autoLoad: true
       });
	    
	    
	   



	    var pluginExpanded = true;
	
	
	    this.dataview = Ext.create('Ext.view.View', {
	        deferInitialRefresh: false,
	        store: this.store,
	        tpl  : Ext.create('Ext.XTemplate',
	            '<tpl for=".">',
	                '<div class="video">',
	                    (!Ext.isIE6? '<img width="307" height="177" src="{srcposter}" />' :
	                     '<div style="width:307px;height:177px;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'{srcposter}\',sizingMethod=\'scale\')"></div>'),
	                    '<strong>{name}</strong>',
	                    '<span>{price:usMoney} ({reviews} Review{[values.reviews == 1 ? "" : "s"]})</span>',
	                '</div>',
	            '</tpl>'
	        ),
	
	        plugins : [
	            Ext.create('Ext.ux.DataView.Animated', {
	                duration  : 550,
	                idProperty: 'id'
	            })
	        ],
	        id: 'videos',
	        itemSelector: 'div.video',
	        overItemCls : 'video-hover',
	        singleSelect: true,
	        autoScroll  : true,
	        layout:'fit',
	        
	        features:[{
		            ftype: 'grouping',
		            hideGroupedHeader: false
		        }]
		    
	    });
	    
	    var t = new Ext.Template("<video id='video_{xid}' class='video-js vjs-default-skin' controls preload='auto'  poster='http://video-js.zencoder.com/oceans-clip.png'>",
	    	                           "<source src='{src}' type='{videotype}'/>",
	    	                           "</video>"
	    	                         );
	    
	    
	    var t_mov = new Ext.Template("<embed  id='video_{xid}' src='videos/mov.mov'",
										"pluginspage='http://www.apple.com/quicktime/download/'", 
										"scale='tofit'",
										"cache=true",
										"WIDTH='100%' HEIGHT = '100%'",
										"controller=True",
										"type='video/quicktime'", 
										"autoplay=true/>");
	    
	     
	    this.dataview.on('itemdblclick',function(view,rec,item,index,e,eOpts){
	    	
	           rec.data.xid = Ext.id();
	    
	           var html;
	           var sw = true;
	           
	           if(rec.data.videotype!='video/quicktime'){
	           	   html = t.apply(rec.data);
	           }else{
	           	   html = t_mov.apply(rec.data);
	           	   sw = false;
	           }
	           
	           this.win = Ext.create( 	'Ext.window.Window', {
					    title:rec.data.name,
					    layout: 'fit',
					    width: 800,
					    height: 500,
					    x: 20,
					    y: 20,
					    resizable: true,
					    maximizable:true,
					    html:html
					    
					});
				this.win.show();
				var size = this.win.getSize();
				this.elvideo = Ext.get('video_'+rec.data.xid);
				
				if(sw){
					this.player = videojs('video_'+rec.data.xid,  { "controls": true, "autoplay": false, "preload": "auto" },function(){});
					
					this.player.dimensions(size.width , size.height-30 )
					var player = this.player
					this.win.on({
					      scope : this,
					       resize : function(panel, width , height ) {
					      	if (player)
					          player.dimensions(width, height-30)
					      }
					    });
				   }
				   else{
				   	
				   	this.elvideo.center(this.win.id)
				   	 this.win.on({
					       scope : this,
					       resize : function(panel, width , height ) {
					       	this.elvideo.center(this.win.id)
				   	       }
					    });
				   }
				
	         },this)
	    
	    this.textFilter = Ext.create('Ext.form.field.Text', {
	        hideLabel: true,
	        name:'filter',
	        listeners: {
	        	scope : this,
	            change: {
	                buffer: 70,
	                fn    : this.filterData
	            }
	        }
	    });
	
	  
	   this.panel = Ext.getCmp(this.idContent);
	   this.comboType  = Ext.create('Ext.form.field.ComboBox',
	             {
                       fieldLabel: 'Filter By',
                        labelAlign: 'right',
                        labelWidth: 45,
                        valueField: 'field',
                        displayField: 'label',
                        value: 'name',
                       
                        editable: false,
                        store: Ext.create('Ext.data.Store', {
                            fields: ['field', 'label'],
                            sorters: 'type',
                            proxy : {
                                type: 'memory',
                                data  : [{label: 'Name', field: 'name'}, {label: 'Price', field: 'price'}]
                            }
                        }),
                        listeners: {
                            scope : this,
                            select: this.filterData
                        }
                    }
	   );
	   
	  
	   this.panelSec= Ext.create('Ext.panel.Panel', {
	        autoScroll:true,
            region:'center',
            plain: true,
          
	        
	        items : this.dataview,
	       
	        tbar  : [
	                 this.comboType,
	            {
                width: 400,
                labelWidth: 50,
                xtype: 'searchfield',
                store: this.store,
                paramName:'name'
                }, '->', {
                xtype: 'component',
                itemId: 'status',
                tpl: 'Matching threads: {count}',
                style: 'margin-right:5px'
                }
	        ],
	        // paging bar on the bottom
	        bbar: Ext.create('Ext.PagingToolbar', {
	            store: this.store,
	            displayInfo: true,
	            displayMsg: 'Displaying topics {0} - {1} of {2}',
	            emptyMsg: "No topics to display",
	        })
	    });
	    
	    this.store.on('load',function(){
	    	
	    	this.dataview.refresh();
	    },this);
	    
	    this.panel.add(this.panelSec);
	 
	
  }	,
   //filters the store based on the current slider values
	filterData:function (field, value) {
		  
	    	this.filedsearch = this.panelSec.down('searchfield');
	    	this.filedsearch.paramName = field.getValue();
	    	if (this.filedsearch.getValue().length > 0) {
	    		this.filedsearch.setValue('');
                this.filedsearch.store.clearFilter();
	    	}
	    	
	    }
	
});
