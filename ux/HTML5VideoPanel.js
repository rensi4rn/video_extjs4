/* -NOTICE-
 * For HTML5 video to work, your server must
 * send the right content type, for more info see:
 * https://developer.mozilla.org/En/HTML/Element/Video
 */

Ext.define('Ext.ux.HTML5VideoPanel',{
  extend:  Ext.window.Window,
  constructor: function(config) {
  	
  	this.callParent(this, Ext.applyIf(config, {
      width    : '100%',
      height   : '100%',
      autoplay : false,
      controls : true,
      bodyStyle: 'background-color:#000;color:#fff',
      html     : '',
      suggestChromeFrame: false
    }));
 
    this.on({
      scope        : this,
      render       : this._render,
      beforedestroy: function() {
        this.video = null;
      },
      bodyresize   : function(panel, width, height) {
        if (this.video)
        this.video.setSize(width, height);
      }
    });
  },
 
  _render: function() {
    var fallback = '';
 
    if (this.fallbackHTML) {
      fallback = this.fallbackHTML;
    } else {
      fallback = "Your browser doesn't support html5 video. ";
 
      if (Ext.isIE && this.suggestChromeFrame) {
        /* chromeframe requires that your site have a special tag in the header
         * see http://code.google.com/chrome/chromeframe/ for details
         */
        fallback += '<a>Get Google Chrome Frame for IE</a>';
      } else if (Ext.isChrome) {
        fallback += '<a>Upgrade Chrome</a>';
      } else if (Ext.isGecko) {
        fallback += '<a>Upgrade to Firefox 3.5</a>';
      } else {
        fallback += '<a>Get Firefox 3.5</a>';
      }
    }
 
    /* match the video size to the panel dimensions */
    var size = this.getSize();
 
    var cfg = Ext.copyTo({
      tag   : 'video',
      width : size.width,
      height: size.height
    },
    this, 'poster,start,loopstart,loopend,playcount,autobuffer,loop');
 
    /* just having the params exist enables them */
    if (this.autoplay) cfg.autoplay = 1;
    if (this.controls) cfg.controls = 1;
 
    /* handle multiple sources */
    if (Ext.isArray(this.src)) {
      cfg.children = [];
 
      for (var i = 0, len = this.src.length; i < len; i++) {
        if (!Ext.isObject(this.src)) {
          throw "source list passed to html5video panel must be an array of objects";
        }
 
        cfg.children.push(
          Ext.applyIf({tag: 'source'}, this.src)
        );
      }
 
      cfg.children.push({
        html: fallback
      });
 
    } else {
      cfg.src  = this.src;
      cfg.html = fallback;
    }
 
    this.video = this.body.createChild(cfg);
  }
 
});